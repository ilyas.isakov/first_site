from django.db import models

# Create your models here.
class Place(models.Model):
    name = models.CharField('name', max_length=100)
    address = models.CharField('address', max_length=200)
    rating = models.DecimalField('rating', max_digits=4, decimal_places=2)

    class Meta:
        verbose_name = 'Место'
        verbose_name_plural = 'Места'