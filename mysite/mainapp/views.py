from django.shortcuts import render, redirect
# Create your views here.
from .models import Place
from .forms import PlaceForm, UserRegistrationForm

def index(request):
    return render(request, 'mainapp/index.html', {'title': 'Main'})

def login(request):
    return render(request, 'mainapp/login.html', {'title': 'Login'})

def rating(request):
    places = list(Place.objects.order_by('rating')[:100])
    places.reverse()
    return render(request, 'mainapp/rating.html', {'title': 'Rating', 'places': places})

def create(request):
    error = ''
    if request.method == 'POST':
        form = PlaceForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
        else:
            error = "WRONG FORM"
    form = PlaceForm()
    context = {
        'form': form,
        'error': error
    }
    return render(request, 'mainapp/create.html', context)


def register(request):
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            # Create a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            # Set the chosen password
            new_user.set_password(user_form.cleaned_data['password'])
            # Save the User object
            new_user.save()
            return render(request, 'account/register_done.html', {'new_user': new_user})
    else:
        user_form = UserRegistrationForm()
    return render(request, 'account/register.html', {'user_form': user_form})

def register_done(request):
    return render(request, 'mainapp/register_done.html', {'title': 'Register_done'})
