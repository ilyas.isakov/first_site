from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('rating/', views.rating, name='rating'),
    path('create/', views.create, name='create'),
    path('register_done/', views.register_done, name='register_done'),
    path('register/', views.register, name='register'),
]