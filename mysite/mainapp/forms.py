from .models import Place
from django.forms import ModelForm, TextInput, CharField, PasswordInput
from django.contrib.auth.models import User

class PlaceForm(ModelForm):
    class Meta:
        model = Place
        fields = ["name", "address", "rating"]
        widgets = {"name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter organization name'
            }),
            "address": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter address'
            }),

            "rating": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter rating'
            })
        }

class UserRegistrationForm(ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'email')
        widgets = {"username": TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'Enter user name'
        }),
            "first_name": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter first name'
            }),

            "email": TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter email'
            }),

            "password": PasswordInput(attrs={
                'class': 'form-control',
                'placeholder': 'Enter password'
            })
        }

